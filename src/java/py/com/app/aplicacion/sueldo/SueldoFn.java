/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.app.aplicacion.sueldo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import nebuleuse.ORM.postgres.Conexion;

/**
 *
 * @author hugo
 */
public class SueldoFn {
    
    public Long monto_total (Integer cedula ) throws SQLException{
    
        Long ret = 0L;
        
        
        
        Conexion conexion = new Conexion();
        
        ResultSet resultset;     
    
        
        conexion.conectar();  
        //statement = conexion.getConexion().createStatement();              
               
        
        String sql = 
                " SELECT sum(remuneracion_total) total \n" +
                " FROM aplicacion.sicca_sueldos \n" +
                " WHERE cedula = " + cedula ; 
        
        
        resultset = conexion.getConexion().createStatement().executeQuery(sql);      

        
        if (resultset.next()){
            ret = resultset.getLong("total");            
        }
            
        conexion.desconectar();
        
        return ret;  
    
    }
    
    
}
