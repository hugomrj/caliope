/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.app.aplicacion.sueldo;


import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Pattern;

/**
 *
 * @author hugo
 */
public class SueldoFile {
    
    String linea_final_pagina = "" ;
    String linea_anterior = "" ;
    Integer contador_lineas = 0;
    
    
    
    
    public void readfile (  String carpeta, String file ) {    
    
        String nombreFichero = carpeta +  file ;
        

        BufferedReader br = null;
        try {

           br = new BufferedReader(new FileReader(nombreFichero));

           String texto = br.readLine();
    
           
           
           while( texto != null )
           {
               
                String sSubCadena = texto.trim();
               
                if (this.contador_lineas != 0){
                    
                    if (sSubCadena.length() !=0 ){
                        leerLinea (sSubCadena ) ;   
                    }
                }
                
                this.contador_lineas++ ;
                
                texto = br.readLine();
                
                //texto = null;
           }
        }
        
                
        
        catch (FileNotFoundException e) {
            System.out.println("Error: Fichero no encontrado");
            System.out.println(e.getMessage());
        }
        catch(Exception e) {
            System.out.println("Error de lectura del fichero");
            System.out.println(e.getMessage());
        }
        
        
        finally {
            try {
                if(br != null)
                    br.close();
            }
            catch (Exception e) {
                System.out.println("Error al cerrar el fichero");
                System.out.println(e.getMessage());
            }
        }
     
    }
    
     
        
        
        
    
    
    
    
    
    
    


    public void leerLinea (String line ) throws IOException {

        new SueldoSQL().insert(line);
    
     //return inicial;
         
    }    
    
    
        
    
    
    
  
 
    
    
}
