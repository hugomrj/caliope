/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.app.aplicacion.siccasueldo;



import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.postgres.Conexion;
import nebuleuse.seguridad.Autentificacion;
import nebuleuse.util.Convercion;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import py.com.app.aplicacion.sueldo.SueldoFn;

/**
 *
 * @author hugo
 */

@WebServlet(name = "SiccaSueldo_CertificadoPDF", 
        urlPatterns = {"/siccasueldo/certificado.pdf"})


public class SiccaSueldo_CertificadoPDF extends HttpServlet {


    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        
        
        
            HttpSession sesion = request.getSession();        
            
            
            
        
        try { 
        
            
                Integer par_cedula = 0;
                par_cedula = Integer.parseInt( request.getParameter("cedula").toString() ) ;
                
                String par_monto_letras = "";
                Long par_monto_total = 9999999L;
                
                
                String fechahora = "";
                fechahora = request.getParameter("datetime").toString();
                
                
                
                /*
                Integer par_agno = 0;
                par_agno = Integer.parseInt( request.getParameter("agno").toString() ) ;
                
                Integer par_mes = 0;
                par_mes = Integer.parseInt( request.getParameter("mes").toString() ) ;
                */
                
                
                
                Autentificacion autorizacion = new Autentificacion();
                String strToken =  (String) sesion.getAttribute("sesiontoken");
                

                
System.err.println("token en reporte");                
System.err.println(strToken);                


                
                if (autorizacion.verificar(strToken))
                {                      

                    
                    String archivo = "siccasueldo_certificado";            
                    String archivo_jrxml = archivo+".jrxml";
                    response.setHeader("Content-disposition","inline; filename="+archivo+".pdf");
                    response.setContentType("application/pdf");



                    Conexion cnx = new Conexion();
                    cnx.conectar();
                    String url =  request.getServletContext().getRealPath("/WEB-INF")+"/jasper/";
                    url = url + archivo_jrxml;

                    
                
                    // obtener la suma de sueldos
                    SueldoFn suedofn = new SueldoFn();
                    par_monto_total = suedofn.monto_total( par_cedula ) ;
            
                    
                    
                    // parametros
                    Map<String, Object> parameters = new HashMap<String, Object>();

                    
                    parameters.put("par_cedula", par_cedula );            
                    parameters.put("par_usuario", autorizacion.token.getNombre() );            
                
                    parameters.put("par_monto_total", par_monto_total );            
                    parameters.put("par_fechahora", fechahora );            
                    
                    
                    Convercion conversion = new Convercion();   
                    parameters.put("par_monto_letras", 
                            conversion.numeroaLetras( par_monto_total )  );
                    
                    
                    /*
                    parameters.put("par_agno", par_agno );            
                    parameters.put("par_mes", par_mes );            
                    */
                    
                    
                    String report_path = request.getServletContext().getRealPath("/WEB-INF")+"/jasper/";;
                    report_path = report_path.replace("\\", "/") ;

                    parameters.put("report_path", report_path );    


                    
                
                    JasperReport report = JasperCompileManager.compileReport(url);            
                    JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, cnx.getConexion());


                    ServletOutputStream servletOutputStream = response.getOutputStream();
                    byte[] reportePdf = JasperExportManager.exportReportToPdf(jasperPrint);

                    response.setContentLength(reportePdf.length);

                    servletOutputStream.write(reportePdf, 0, reportePdf.length);
                    servletOutputStream.flush();
                    servletOutputStream.close();    

                    
                    response.setStatus(HttpServletResponse.SC_ACCEPTED);
                }
                else
                {  
                    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                }
                
                
//                autorizacion.actualizar();


            
            
            
        }         
        catch (JRException ex) 
        {
            Logger.getLogger(SiccaSueldo_CertificadoPDF.class.getName()).log(Level.SEVERE, null, ex);
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        } catch (Exception ex) {
            Logger.getLogger(SiccaSueldo_CertificadoPDF.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {          
            sesion.invalidate();
        }

            
            
            
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(SiccaSueldo_CertificadoPDF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (Exception ex) {
            Logger.getLogger(SiccaSueldo_CertificadoPDF.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
