/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


package py.com.utilidad.migracion;

import com.google.gson.Gson;
import java.util.List;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.UriInfo;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import nebuleuse.ORM.Persistencia;
import nebuleuse.ORM.xml.Global;
import nebuleuse.seguridad.Autentificacion;
import nebuleuse.util.Webinf;
import py.com.app.aplicacion.sueldo.SueldoMigracion;


/**
 * REST Web Service
 * @author hugo
 */
           

           
@Path("migracion")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)



public class MigracionWS {

    @Context
    private UriInfo context;    
    private Persistencia persistencia = new Persistencia();   
    private Autentificacion autorizacion = new Autentificacion();
    private Gson gson = new Gson();          
    private Response.Status status  = Response.Status.OK;
    
    
    
                         
    public MigracionWS() {
        
    }

    
    

    
    @POST
    //@Path("/proceso")
    @Path("/migrarsueldos")
    public  Response proceso (  @HeaderParam("token") String strToken ) {
        
                    
        try {
            
            
            
             Persistencia persistencia = new Persistencia();
             
             
            
            Webinf webinf = new Webinf();
            webinf.setCarpeta("files");
            webinf.ini();
            
            
            //String ruta = "/home/hugo/Descargas/bellas/";
            String ruta = webinf.getPath();
            
            Migracion migracion = new Migracion();
            
            migracion.setCarpeta(ruta);
            migracion.setFicheros();
            
            
            persistencia.ejecutarSQL (" DELETE FROM aplicacion.sicca_sueldos; ") ;            
            
            SueldoMigracion sueldomigra = new SueldoMigracion();            
            sueldomigra.migrardatos(migracion);
            
            
            
            
            return Response
                    .status(Response.Status.OK)
                    .entity("true")
                    .header("token", strToken )
                    .build();                        

        }                      
         
        
        catch (Exception ex) {
            
            return Response
                    .status(Response.Status.INTERNAL_SERVER_ERROR)
                    .build();                                        
        }
    }
       

    

        
    
    
    

    
}


