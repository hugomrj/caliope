/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nebuleuse.ORM.xml;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *
 * @author hugo
 */
public class FactoryXML {
    
    
    private ArrayList<FactoryLine> lineas = new ArrayList<FactoryLine>(); 
    private String xmldeclaracion = "";
     
    
    
    public void add (String linea){    
                
        // colocar algun controlador
        FactoryLine fline = new FactoryLine();
        fline.setLinea(linea);
        
        fline.setCierre('n');
                
        this.getLineas().add(fline);
                
    }
    
    

    
    
    
 
    public void readfile (  File file ) {
    
        BufferedReader br = null;
        try {

           //br = new BufferedReader(new FileReader(nombreFichero));
           file.createNewFile();           
           //Reader targetReader = new FileReader(initialFile);
           
           br = new BufferedReader(new FileReader(file));

           String texto = br.readLine();
           
           while(texto != null)
           {               
                String sSubCadena = texto.trim();
                
                if (sSubCadena.length() != 0 ){                                                    
                    //planfinanciero = leerLinea (sSubCadena, planfinanciero ) ;                          
                    //System.out.println( sSubCadena );    
                    
                    this.add(sSubCadena);                    
                }                              
               texto = br.readLine();               
           }
           

           if (this.lineas.size() > 0){               
               // falta controlar si empieza con <?xml
               // solo para la primera linea
               this.xmldeclaracion = this.lineas.get(0).getLinea();
               this.lineas.remove(0);               
           }
           
           
        }
        
                
        
        catch (FileNotFoundException e) {
            System.out.println("Error: Fichero no encontrado");
            System.out.println(e.getMessage());
        }
        catch(Exception e) {
            System.out.println("Error de lectura del fichero");
            System.out.println(e.getMessage());
        }
        
        
        finally {
            try {
                if(br != null)
                    br.close();
            }
            catch (Exception e) {
                System.out.println("Error al cerrar el fichero");
                System.out.println(e.getMessage());
            }
        }     
    }
    
    
    
    
   /* 
    public  Documento parseXml (){
    
        Documento doc = new Documento();        
        Nodo nodoRoot = new Nodo();             
        
        ArrayList<Nodo> nodos = new ArrayList <Nodo>() ;
        
        Nodo n1 = new Nodo();
        n1.setNombre("dfdasfasf");
        nodos.add(n1);
                
        n1.setNombre("dfasfa");
        nodos.add(n1);
        
        n1.setNombre("sdfsaf");
        nodos.add(n1);        
        
        n1.setNombre("sdfsafsadgag");
        nodos.add(n1);        
                


            
        // ver aca si ya empezar con la recursividad
        // o si se va a ver a llamar a otra funcion
                
        
        nodoRoot=  this.getNode();
            
            

            
            
            
//nodoRoot.setNodos(nodos);                    
                    
        doc.setNodo(nodoRoot);
                    
        //doc.setNodos(nodos);        
        
        return doc;
            
    }
    
     */   
  
    
    
    
   /* 
    public Nodo getNode() {
    
        Nodo nodo = new Nodo();
        String nombre = "";
        
        
        
        System.out.println("-------------nodo----");        
                       
      ///              /^<[a-zA-Z]+\/>$/           
        
        
        return nodo;
    }
    */
    
    
    public int charCant(String str, char ocu) {
        
        int r = 0;
        

        for (char c: str.toCharArray ()) { 
            if (c == ocu){
                r++;
            }
        }        
        
        
        
        return r;
    }    

    public ArrayList<FactoryLine> getLineas() {
        return lineas;
    }
    
    
}


