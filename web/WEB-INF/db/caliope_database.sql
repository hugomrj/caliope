--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

-- Started on 2020-11-04 14:51:45

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 153900)
-- Name: aplicacion; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA aplicacion;


ALTER SCHEMA aplicacion OWNER TO postgres;

--
-- TOC entry 8 (class 2615 OID 153901)
-- Name: sistema; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA sistema;


ALTER SCHEMA sistema OWNER TO postgres;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2194 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = aplicacion, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 198 (class 1259 OID 153990)
-- Name: sicca_sueldos; Type: TABLE; Schema: aplicacion; Owner: postgres
--

CREATE TABLE sicca_sueldos (
    id integer NOT NULL,
    agno integer,
    mes integer,
    linea integer,
    cedula integer,
    nombres character varying,
    apellidos character varying,
    remuneracion_total bigint,
    objeto_gasto integer,
    categoria character varying,
    cargo character varying
);


ALTER TABLE sicca_sueldos OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 153988)
-- Name: sicca_sueldos_id_seq; Type: SEQUENCE; Schema: aplicacion; Owner: postgres
--

CREATE SEQUENCE sicca_sueldos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE sicca_sueldos_id_seq OWNER TO postgres;

--
-- TOC entry 2195 (class 0 OID 0)
-- Dependencies: 197
-- Name: sicca_sueldos_id_seq; Type: SEQUENCE OWNED BY; Schema: aplicacion; Owner: postgres
--

ALTER SEQUENCE sicca_sueldos_id_seq OWNED BY sicca_sueldos.id;


SET search_path = sistema, pg_catalog;

--
-- TOC entry 183 (class 1259 OID 153903)
-- Name: roles; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE roles (
    rol integer NOT NULL,
    nombre_rol character varying(140)
);


ALTER TABLE roles OWNER TO postgres;

--
-- TOC entry 184 (class 1259 OID 153906)
-- Name: roles_rol_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE roles_rol_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE roles_rol_seq OWNER TO postgres;

--
-- TOC entry 2196 (class 0 OID 0)
-- Dependencies: 184
-- Name: roles_rol_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE roles_rol_seq OWNED BY roles.rol;


--
-- TOC entry 185 (class 1259 OID 153908)
-- Name: roles_x_selectores; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE roles_x_selectores (
    id integer NOT NULL,
    rol integer,
    selector integer
);


ALTER TABLE roles_x_selectores OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 153911)
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE roles_x_selectores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE roles_x_selectores_id_seq OWNER TO postgres;

--
-- TOC entry 2197 (class 0 OID 0)
-- Dependencies: 186
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE roles_x_selectores_id_seq OWNED BY roles_x_selectores.id;


--
-- TOC entry 187 (class 1259 OID 153913)
-- Name: selectores; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE selectores (
    id integer NOT NULL,
    superior integer,
    descripcion character varying,
    ord integer,
    link character varying
);


ALTER TABLE selectores OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 153919)
-- Name: selectores_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE selectores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE selectores_id_seq OWNER TO postgres;

--
-- TOC entry 2198 (class 0 OID 0)
-- Dependencies: 188
-- Name: selectores_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE selectores_id_seq OWNED BY selectores.id;


--
-- TOC entry 189 (class 1259 OID 153921)
-- Name: selectores_x_webservice; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE selectores_x_webservice (
    id integer NOT NULL,
    selector integer,
    wservice integer
);


ALTER TABLE selectores_x_webservice OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 153924)
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE selectores_x_webservice_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE selectores_x_webservice_id_seq OWNER TO postgres;

--
-- TOC entry 2199 (class 0 OID 0)
-- Dependencies: 190
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE selectores_x_webservice_id_seq OWNED BY selectores_x_webservice.id;


--
-- TOC entry 191 (class 1259 OID 153926)
-- Name: usuarios; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE usuarios (
    usuario integer NOT NULL,
    cuenta character varying(100),
    clave character varying(150),
    token_iat character varying
);


ALTER TABLE usuarios OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 153932)
-- Name: usuarios_usuario_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE usuarios_usuario_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuarios_usuario_seq OWNER TO postgres;

--
-- TOC entry 2200 (class 0 OID 0)
-- Dependencies: 192
-- Name: usuarios_usuario_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE usuarios_usuario_seq OWNED BY usuarios.usuario;


--
-- TOC entry 193 (class 1259 OID 153934)
-- Name: usuarios_x_roles; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE usuarios_x_roles (
    id integer NOT NULL,
    usuario integer,
    rol integer
);


ALTER TABLE usuarios_x_roles OWNER TO postgres;

--
-- TOC entry 194 (class 1259 OID 153937)
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE usuarios_x_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE usuarios_x_roles_id_seq OWNER TO postgres;

--
-- TOC entry 2201 (class 0 OID 0)
-- Dependencies: 194
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE usuarios_x_roles_id_seq OWNED BY usuarios_x_roles.id;


--
-- TOC entry 195 (class 1259 OID 153939)
-- Name: webservice; Type: TABLE; Schema: sistema; Owner: postgres
--

CREATE TABLE webservice (
    wservice integer NOT NULL,
    path character varying,
    nombre character varying
);


ALTER TABLE webservice OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 153945)
-- Name: webservice_wservice_seq; Type: SEQUENCE; Schema: sistema; Owner: postgres
--

CREATE SEQUENCE webservice_wservice_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE webservice_wservice_seq OWNER TO postgres;

--
-- TOC entry 2202 (class 0 OID 0)
-- Dependencies: 196
-- Name: webservice_wservice_seq; Type: SEQUENCE OWNED BY; Schema: sistema; Owner: postgres
--

ALTER SEQUENCE webservice_wservice_seq OWNED BY webservice.wservice;


SET search_path = aplicacion, pg_catalog;

--
-- TOC entry 2036 (class 2604 OID 153993)
-- Name: id; Type: DEFAULT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY sicca_sueldos ALTER COLUMN id SET DEFAULT nextval('sicca_sueldos_id_seq'::regclass);


SET search_path = sistema, pg_catalog;

--
-- TOC entry 2029 (class 2604 OID 153947)
-- Name: rol; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY roles ALTER COLUMN rol SET DEFAULT nextval('roles_rol_seq'::regclass);


--
-- TOC entry 2030 (class 2604 OID 153948)
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY roles_x_selectores ALTER COLUMN id SET DEFAULT nextval('roles_x_selectores_id_seq'::regclass);


--
-- TOC entry 2031 (class 2604 OID 153949)
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY selectores ALTER COLUMN id SET DEFAULT nextval('selectores_id_seq'::regclass);


--
-- TOC entry 2032 (class 2604 OID 153950)
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY selectores_x_webservice ALTER COLUMN id SET DEFAULT nextval('selectores_x_webservice_id_seq'::regclass);


--
-- TOC entry 2033 (class 2604 OID 153951)
-- Name: usuario; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY usuarios ALTER COLUMN usuario SET DEFAULT nextval('usuarios_usuario_seq'::regclass);


--
-- TOC entry 2034 (class 2604 OID 153952)
-- Name: id; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY usuarios_x_roles ALTER COLUMN id SET DEFAULT nextval('usuarios_x_roles_id_seq'::regclass);


--
-- TOC entry 2035 (class 2604 OID 153953)
-- Name: wservice; Type: DEFAULT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY webservice ALTER COLUMN wservice SET DEFAULT nextval('webservice_wservice_seq'::regclass);


SET search_path = aplicacion, pg_catalog;

--
-- TOC entry 2186 (class 0 OID 153990)
-- Dependencies: 198
-- Data for Name: sicca_sueldos; Type: TABLE DATA; Schema: aplicacion; Owner: postgres
--

COPY sicca_sueldos (id, agno, mes, linea, cedula, nombres, apellidos, remuneracion_total, objeto_gasto, categoria, cargo) FROM stdin;
1	2020	8	21000	119746	OSVALDO LAUREANO	GONZALEZ REAL	5206670	111	ZZ9	DOCENTE
\.


--
-- TOC entry 2203 (class 0 OID 0)
-- Dependencies: 197
-- Name: sicca_sueldos_id_seq; Type: SEQUENCE SET; Schema: aplicacion; Owner: postgres
--

SELECT pg_catalog.setval('sicca_sueldos_id_seq', 1, true);


SET search_path = sistema, pg_catalog;

--
-- TOC entry 2171 (class 0 OID 153903)
-- Dependencies: 183
-- Data for Name: roles; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY roles (rol, nombre_rol) FROM stdin;
9	SysAdmin
\.


--
-- TOC entry 2204 (class 0 OID 0)
-- Dependencies: 184
-- Name: roles_rol_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('roles_rol_seq', 12, true);


--
-- TOC entry 2173 (class 0 OID 153908)
-- Dependencies: 185
-- Data for Name: roles_x_selectores; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY roles_x_selectores (id, rol, selector) FROM stdin;
3	9	36
4	9	2
5	9	3
2	9	11
35	9	57
112	9	96
\.


--
-- TOC entry 2205 (class 0 OID 0)
-- Dependencies: 186
-- Name: roles_x_selectores_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('roles_x_selectores_id_seq', 112, true);


--
-- TOC entry 2175 (class 0 OID 153913)
-- Dependencies: 187
-- Data for Name: selectores; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY selectores (id, superior, descripcion, ord, link) FROM stdin;
3	1	Roles	2	/sistema/rol/
11	1	Acceso menu	3	/sistema/selector/
36	1	Salir	7	/
1	0	Sistema	10	
57	1	Cambio de password	4	/aplicacion/cambiopass/
2	1	Usuarios	1	/sistema/usuario/
95	0	Aplicacion	20	
96	95	Sueldo sicca	10	/aplicacion/siccasueldo/
\.


--
-- TOC entry 2206 (class 0 OID 0)
-- Dependencies: 188
-- Name: selectores_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('selectores_id_seq', 96, true);


--
-- TOC entry 2177 (class 0 OID 153921)
-- Dependencies: 189
-- Data for Name: selectores_x_webservice; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY selectores_x_webservice (id, selector, wservice) FROM stdin;
\.


--
-- TOC entry 2207 (class 0 OID 0)
-- Dependencies: 190
-- Name: selectores_x_webservice_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('selectores_x_webservice_id_seq', 1, false);


--
-- TOC entry 2179 (class 0 OID 153926)
-- Dependencies: 191
-- Data for Name: usuarios; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY usuarios (usuario, cuenta, clave, token_iat) FROM stdin;
1	root	63a9f0ea7bb98050796b649e85481845	1604512245512
\.


--
-- TOC entry 2208 (class 0 OID 0)
-- Dependencies: 192
-- Name: usuarios_usuario_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('usuarios_usuario_seq', 5, true);


--
-- TOC entry 2181 (class 0 OID 153934)
-- Dependencies: 193
-- Data for Name: usuarios_x_roles; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY usuarios_x_roles (id, usuario, rol) FROM stdin;
197	1	9
\.


--
-- TOC entry 2209 (class 0 OID 0)
-- Dependencies: 194
-- Name: usuarios_x_roles_id_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('usuarios_x_roles_id_seq', 206, true);


--
-- TOC entry 2183 (class 0 OID 153939)
-- Dependencies: 195
-- Data for Name: webservice; Type: TABLE DATA; Schema: sistema; Owner: postgres
--

COPY webservice (wservice, path, nombre) FROM stdin;
\.


--
-- TOC entry 2210 (class 0 OID 0)
-- Dependencies: 196
-- Name: webservice_wservice_seq; Type: SEQUENCE SET; Schema: sistema; Owner: postgres
--

SELECT pg_catalog.setval('webservice_wservice_seq', 1, false);


SET search_path = aplicacion, pg_catalog;

--
-- TOC entry 2052 (class 2606 OID 153998)
-- Name: sicca_sueldos_pkey; Type: CONSTRAINT; Schema: aplicacion; Owner: postgres
--

ALTER TABLE ONLY sicca_sueldos
    ADD CONSTRAINT sicca_sueldos_pkey PRIMARY KEY (id);


SET search_path = sistema, pg_catalog;

--
-- TOC entry 2038 (class 2606 OID 153955)
-- Name: roles_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (rol);


--
-- TOC entry 2040 (class 2606 OID 153957)
-- Name: roles_x_selectores_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_pkey PRIMARY KEY (id);


--
-- TOC entry 2042 (class 2606 OID 153959)
-- Name: selectores_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY selectores
    ADD CONSTRAINT selectores_pkey PRIMARY KEY (id);


--
-- TOC entry 2044 (class 2606 OID 153961)
-- Name: selectores_x_webservice_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY selectores_x_webservice
    ADD CONSTRAINT selectores_x_webservice_pkey PRIMARY KEY (id);


--
-- TOC entry 2046 (class 2606 OID 153963)
-- Name: usuarios_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY usuarios
    ADD CONSTRAINT usuarios_pkey PRIMARY KEY (usuario);


--
-- TOC entry 2048 (class 2606 OID 153965)
-- Name: usuarios_x_roles_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2050 (class 2606 OID 153967)
-- Name: webservice_pkey; Type: CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY webservice
    ADD CONSTRAINT webservice_pkey PRIMARY KEY (wservice);


--
-- TOC entry 2053 (class 2606 OID 153968)
-- Name: roles_x_selectores_rol_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_rol_fkey FOREIGN KEY (rol) REFERENCES roles(rol);


--
-- TOC entry 2054 (class 2606 OID 153973)
-- Name: roles_x_selectores_selector_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY roles_x_selectores
    ADD CONSTRAINT roles_x_selectores_selector_fkey FOREIGN KEY (selector) REFERENCES selectores(id);


--
-- TOC entry 2055 (class 2606 OID 153978)
-- Name: usuarios_x_roles_rol_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_rol_fkey FOREIGN KEY (rol) REFERENCES roles(rol);


--
-- TOC entry 2056 (class 2606 OID 153983)
-- Name: usuarios_x_roles_usuario_fkey; Type: FK CONSTRAINT; Schema: sistema; Owner: postgres
--

ALTER TABLE ONLY usuarios_x_roles
    ADD CONSTRAINT usuarios_x_roles_usuario_fkey FOREIGN KEY (usuario) REFERENCES usuarios(usuario);


--
-- TOC entry 2193 (class 0 OID 0)
-- Dependencies: 9
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2020-11-04 14:51:48

--
-- PostgreSQL database dump complete
--

